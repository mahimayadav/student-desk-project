### Installation

 - Clone this project
 - Install module dependencies by run this script in terminal
    `npm install`

 - Finally run the app.
	 `npm start`
 - Navigate to  [http://localhost:3001](http://localhost:3001)


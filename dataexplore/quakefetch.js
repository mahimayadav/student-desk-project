const fetch = require("node-fetch")

const url ="https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2014-01-01&endtime=2014-01-02"

fetch(url)
  .then(function(response) {
   return response.json();
  })
  .then(function(data){
    // return console.log(data);

    const quake = data.features[0];
    const date = new Date(quake.properties.time)
    console.log(date)
    const year = date.getFullYear();
    const month = monthName(date.getMonth())
    // console.log(month)
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    const seconds = date.getSeconds()
    const datestrings = `${month} ${day}, ${year} at ${hour}:${minute} and ${seconds} seconds`;
    const timestamp = quake.properties.time

    console.log(datestrings)

    function monthName(index) {
        const monthLegend = {
            0: "january",
            1: "Fabuary",
            2: "March",
            3: "April",
            4: "May",
            5: "June",
            6: "July",
            7: "August",
            8: "September",
            9: "October",
            10: "November",
            11: "December"
        }
        return monthLegend[index];
    };

    const customDate = {
        magnitude: quake.properties.mag,
        location: quake.properties.place,
        when: datestrings,
        time: quake.properties.time,
        id: quake.id
    }
    console.log(customDate)


  });
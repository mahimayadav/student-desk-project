/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'edumations.containers.LocaleToggle';

export default defineMessages({
  en: {
    id: `${scope}.en`,
    defaultMessage: 'English',
  },
  hi: {
    id: `${scope}.id`,
    defaultMessage: 'Hindi',
  },
  ar: {
    id: `${scope}.ar`,
    defaultMessage: 'العربيّة',
  },
});

import Loadable from 'react-loadable';
import Loading from 'enl-components/MainComponents/Loading';

export const DashboardPage = Loadable({
  loader: () => import('./Pages/Dashboard'),
  loading: Loading,
});
// Timeline
export const Timeline = Loadable({
  loader: () => import('./Pages/Timeline'),
  loading: Loading,
});

// // Resources
// export const Resources = Loadable({
//   loader: () => import('./Pages/Resources'),
//   loading: Loading,
// });
// Students Registration
export const AddNewRegistration = Loadable({
  loader: () => import('./Pages/Students/AddNewRegistration'),
  loading: Loading,
});
export const EditRegistration = Loadable({
  loader: () => import('./Pages/Students/EditRegistration'),
  loading: Loading,
});
export const RegistrationList = Loadable({
  loader: () => import('./Pages/Students/RegistrationList'),
  loading: Loading,
});

// Student Details
export const AllStudentDetails = Loadable({
  loader: () => import('./Pages/Students/StudentInfo/AllStudentDetails'),
  loading: Loading,
});

//view Student Details
export const ViewStudentDetails = Loadable({
  loader: () => import('./Pages/Students/StudentInfo/ViewStudentDetails'),
  loading: Loading,
});

//view Student Details
export const EditStudentDetails = Loadable({
  loader: () => import('./Pages/Students/StudentInfo/EditStudentDetails'),
  loading: Loading,
});
// Upload Student documents
export const UploadStudentFile = Loadable({
  loader: () => import('./Pages/Students/forms/UploadStudentFile'),
  loading: Loading,
});

// Upload Students Image
export const ImageUpload = Loadable({
  loader: () => import('./Pages/Students/StudentInfo/ImageUpload'),
  loading: Loading,
});

// // Transport
// export const Transport = Loadable({
//   loader: () => import('./Pages/Transport'),
//   loading: Loading,
// });
// // Calender
// export const AcademicCalender = Loadable({
//   loader: () => import('./Pages/AcademicCalender'),
//   loading: Loading,
// });

// Employees
export const AllEmpList = Loadable({
  loader: () => import('./Pages/Employees/AllEmpList'),
  loading: Loading,
});
//view Employee Details
export const ViewEmpDetails = Loadable({
  loader: () => import('./Pages/Employees/EmployeeInfo/ViewEmpDetails'),
  loading: Loading,
});

//Edit Employee Details
export const EditEmpDetails = Loadable({
  loader: () => import('./Pages/Employees/EmployeeInfo/EditEmpDetails'),
  loading: Loading,
});
// // Exams
// export const Exams = Loadable({
//   loader: () => import('./Pages/Exams'),
//   loading: Loading,
// });
// // Finance
// export const Finance = Loadable({
//   loader: () => import('./Pages/Finance'),
//   loading: Loading,
// });
// // Library
// export const Library = Loadable({
//   loader: () => import('./Pages/Library'),
//   loading: Loading,
// });
// // Inventory
// export const Inventory = Loadable({
//   loader: () => import('./Pages/Inventory'),
//   loading: Loading,
// });
// // Reception
// export const Reception = Loadable({
//   loader: () => import('./Pages/Reception'),
//   loading: Loading,
// });


// Authentication Starts
export const Login = Loadable({
  loader: () => import('./Pages/Authentication/Login'),
  loading: Loading,
});
export const LockScreen = Loadable({
  loader: () => import('./Pages/Authentication/LockScreen'),
  loading: Loading,
});
export const ResetPassword = Loadable({
  loader: () => import('./Pages/Authentication/ResetPassword'),
  loading: Loading,
});
// Authentication Ends


// Error And Maintenance Starts

export const ComingSoon = Loadable({
  loader: () => import('./Pages/Others/ComingSoon'),
  loading: Loading,
});
export const Error = Loadable({
  loader: () => import('./Pages/Others/Error'),
  loading: Loading,
});
export const Maintenance = Loadable({
  loader: () => import('./Pages/Others/Maintenance'),
  loading: Loading,
});
export const NotFound = Loadable({
  loader: () => import('./Pages/Others/NotFound'),
  loading: Loading,
});

// Error And Maintenance Ends


// Aacademic sessions Starts
// Academic add Sessions 
export const AddAcademicSessions = Loadable({
  loader: () => import('./Pages/Academics/AddAcademicSessions'),
  loading: Loading,
});
export const EditAcademicSession = Loadable({
  loader: () => import('./Pages/Academics/EditAcademicSession'),
  loading: Loading,
});

// Academic All Sessions table
export const AllAcademicSessions = Loadable({
  loader: () => import('./Pages/Academics/AllAcademicSessions'),
  loading: Loading,
});
// Academic Add class 
export const AddAcademicClasses = Loadable({
  loader: () => import('./Pages/Academics/Classes/AddAcademicClasses'),
  loading: Loading,
});
// Academic All Classes table
export const AllAcademicClasses = Loadable({
  loader: () => import('./Pages/Academics/Classes/AllAcademicClasses'),
  loading: Loading,
});

// Academic time table 
export const TimeTable = Loadable({
  loader: () => import('./Pages/Academics/TimeTable/TimeTable'),
  loading: Loading,
});

//  Add Academic time table 
export const AddNewTimeTable = Loadable({
  loader: () => import('./Pages/Academics/TimeTable/AddNewTimeTable'),
  loading: Loading,
});

// Allocation time table
export const AllocationTimeTable = Loadable({
  loader: () => import('./Pages/Academics/TimeTable/AllocationTimeTable'),
  loading: Loading,
});
// Aacademic sessions Ends

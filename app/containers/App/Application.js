import React from 'react';
import { PropTypes } from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import Dashboard from '../UIConfig/Dashboard';
import {
  DashboardPage,
  Timeline,
  Error,
  NotFound,
  // Academic
  AllAcademicSessions,
  AddAcademicSessions,
  EditAcademicSession,
  AddAcademicClasses,
  AllAcademicClasses,
  TimeTable,
  AddNewTimeTable,
  AllocationTimeTable,
  //Student
  AddNewRegistration,
  RegistrationList,
  EditRegistration,
  AllStudentDetails,
  UploadStudentFile,
  ImageUpload,
  ViewStudentDetails,
  EditStudentDetails,
  //Employee
  AllEmpList,
  ViewEmpDetails,
  EditEmpDetails

} from '../pageListAsync';



class Application extends React.Component {
  render() {
    const { changeMode, history } = this.props;
    return (
      <Dashboard history={history} changeMode={changeMode}>
        <Switch>
          { /* Home */ }
          <Redirect exact path="/app" to="/app/timeline" push />
          <Route exact path="/app/timeline" component={Timeline} />
          <Route exact path="/app/dashboard" component={DashboardPage} />
          {/* Student */}
          <Route exact path="/app/student/edit-registration" component={EditRegistration} />
          <Route exact path="/app/student/registration-list" component={RegistrationList} />
          <Route exact path="/app/student/add-registration" component={AddNewRegistration} />
          <Route exact path="/app/student/student-list" component={AllStudentDetails} />
          <Route exact path="/app/student/view-details" component={ViewStudentDetails} />
          <Route exact path="/app/student/edit-details" component={EditStudentDetails} />
          <Route exact path="/app/student/upload-file" component={UploadStudentFile} />
          <Route exact path="/app/student/upload-image" component={ImageUpload} />
          {/* Academic */}
          <Route exact path="/app/academic/timetable/create" component={AddNewTimeTable} />
          <Route exact path="/app/academic/timetable/allocate" component={AllocationTimeTable} />
          <Route exact path="/app/academic/timetable" component={TimeTable} />
          <Route exact path="/app/academic/academic-sessions" component={AllAcademicSessions} />
          <Route exact path="/app/academic/add-session" component={AddAcademicSessions} />
          <Route exact path="/app/academic/edit-session" component={EditAcademicSession} />
          <Route exact path="/app/academic/add-class" component={AddAcademicClasses} />
          <Route path="/app/academic/all-classes" component={AllAcademicClasses} />
          {/* Employee */}
          <Route exact path="/app/employee/employee-list" component={AllEmpList} />
          <Route exact path="/app/employee/view-details" component={ViewEmpDetails} />
          <Route exact path="/app/employee/edit-details" component={EditEmpDetails} />
          <Route exact path="/app/pages/not-found" component={NotFound} />
          <Route exact path="/app/pages/error" component={Error} />
          <Route component={NotFound} />
        </Switch>
      </Dashboard>
    );
  }
}

Application.propTypes = {
  changeMode: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default Application;

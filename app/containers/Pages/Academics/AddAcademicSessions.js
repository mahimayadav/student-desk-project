import React from "react";
import ReactDOM from "react-dom";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";

import DateFnsUtils from "@date-io/date-fns";


const styles = () => ({
  card: {
    maxWidth: 420,
    marginTop: 50
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});

 
const form = props => {
  const minDate = new Date(Date.now());
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <h1>Session Form</h1>
            <TextField
              id="sessionName"
              label="Session Name"
              value={values.sessionName}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.sessionName ? errors.sessionName : ""}
              error={touched.sessionName && Boolean(errors.sessionName)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  margin="dense"
                  variant="outlined"
                  fullWidth
                  id="startDate"
                  label="Start Date"
                  format="MM/dd/yyyy"
                  minDate={minDate}
                  onBlur={handleBlur}
                  value={values.startDate}
                  onChange={date => setFieldValue('startDate', date)}
                  helperText={touched.startDate ? errors.startDate : ""}
                  error={touched.startDate && Boolean(errors.startDate)}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}

                />
            </MuiPickersUtilsProvider>
             <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  margin="dense"
                  variant="outlined"
                  fullWidth
                  onBlur={handleBlur}
                  id="endDate"
                  label="End Date"
                  format="MM/dd/yyyy"
                  minDate={minDate}
                  value={values.endDate}
                  onChange={date => setFieldValue('endDate', date)}
                  helperText={touched.endDate ? errors.endDate : ""}
                  error={touched.endDate && Boolean(errors.endDate)}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}

                />
            </MuiPickersUtilsProvider>
            <TextField
              id="description"
              label="Description"
              value={values.description}
              onChange={handleChange}
              onBlur={handleBlur}
              multiline={trueBool}
              rows={4}
              helperText={touched.description ? errors.description : ""}
              error={touched.description && Boolean(errors.description)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
           
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              Submit
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    sessionName,
    description,
    startDate,
    endDate
  }) => {
    return {
      sessionName: sessionName || "",
      description: description || "",
      startDate: startDate || "",
      endDate: endDate || ""
     
    };
  },

  validationSchema: Yup.object().shape({
    sessionName: Yup.string().required("session name is required"),
    description: Yup.string().required(" description required"),
    startDate: Yup.date().required("start date required"),
    endDate: Yup.date().required("end date required")
   
  }),


  handleSubmit:  (values, actions) => {
    setTimeout(() => {
      // submit to the server
      
      console.log(values);
      console.log(actions);
      actions.setSubmitting(true);
      const { history } = this.props;
      history.push('/app/academic/academic-sessions')
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
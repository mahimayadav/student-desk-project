import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles, Divider } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";

import DateFnsUtils from "@date-io/date-fns";


const styles = () => ({
  card: {
    maxWidth: 1000,
    marginTop: 20,
    marginleft:40
  },
  container: {
    display: "auto",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});

const batchCategory =[
    {
        value: "nurserySection",
        label: "Nursery Section "
      },
      {
        value: "lkgSection",
        label: " LKG Section "
      },
      {
        value: "ukgSection",
        label: "UKG Section"
      }
];

const classTiming = [
    {
        value: "morning1stClass ",
        label: " 9:30 to 10:30"
    },
    {
        value: "2ndClass",
        label: "10:30 to 11:30"
    },
    {
        value: "3rdClass",
        label: "11:30 to 12:30"
    },
    {
        value: "4thClass",
        label: "1:30 to 2:30"
    },
    {
        value: "5thClass",
        label: "2:30 to 3:30"
    },
    {
        value: "6thClass",
        label: "3:30 to 4:30"
    },
];
 
const form = props => {
 const [checked, setChecked] = React.useState(['true']);
 const minDate = new Date(Date.now());
  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
                <h1>Time Table</h1>
                <Grid container spacing={2}>
                     
                    <Grid item xs={4}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                margin="dense"
                                variant="outlined"
                                fullWidth
                                onBlur={handleBlur}
                                id="dateEffective"
                                label="Date Effective"
                                format="MM/dd/yyyy"
                                minDate={minDate}
                                value={values.dateEffective}
                                onChange={date => setFieldValue('dateEffective', date)}
                                helperText={touched.dateEffective ? errors.dateEffective : ""}
                                error={touched.dateEffective && Boolean(errors.dateEffective)}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            select
                            id="batch"
                            label="Batch"
                            value={values.batch}
                            onChange={handleChange("batch")}
                            onBlur={handleBlur}
                            helperText={touched.batch ? errors.batch : ""}
                            error={touched.batch && Boolean(errors.batch)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        >
                            {batchCategory.map(option => (
                                <MenuItem key={option.value} value={option.value}>
                                {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            id="description"
                            label="Description"
                            value={values.description}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            multiline={trueBool}
                            rows={2}
                            helperText={touched.description ? errors.description : ""}
                            error={touched.description && Boolean(errors.description)}
                            margin="dense"
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>

                    <Grid container spacing={4}>
                           <Grid item xs={6}>
                               {/* Sunday start */}
                               <Grid item xs={12}>
                                    <Grid item xs={6}>
                                        <label id="switch-list-label-sunday">Sunday</label>
                                        <Switch
                                        edge="end"
                                        onChange={handleToggle('sunday')}
                                        checked={checked.indexOf('sunday') !== -1}
                                        imputProps={{ 'area-labelledby': 'switch-list-label-sunday'}}
                                        />Is Weekoff?
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField
                                            select
                                        id="sundayBatch"
                                        label="Class Timing"
                                        value={values.sundayBatch}
                                        onChange={handleChange("sundayBatch")}
                                        
                                        onBlur={handleBlur}
                                        helperText={touched.sundayBatch ? errors.sundayBatch : ""}
                                        error={touched.sundayBatch && Boolean(errors.sundayBatch)}
                                        margin="dense"
                                        variant="outlined"
                                        fullWidth
                                        
                                        >
                                        {classTiming.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                            </MenuItem>
                                        ))}
                                        </TextField>
                                    </Grid>
                                    
                                </Grid>
                             {/* end */}
                             {/* Monday Start */}
                                <Grid item xs={12}>
                                    <Grid item xs={6}>
                                        <label id="switch-list-label-sunday">Monday</label>
                                        <Switch
                                        edge="end"
                                        onChange={handleToggle('monday')}
                                        checked={checked.indexOf('monday') !== -1}
                                        imputProps={{ 'area-labelledby': 'switch-list-label-monday'}}
                                        />Is Weekoff?
                                    </Grid>
                                    <Grid item xs={12}>
                                <TextField
                                    select
                                id="mondayBatch"
                                label="Class Timing"
                                value={values.mondayBatch}
                                onChange={handleChange("mondayBatch")}
                                onBlur={handleBlur}
                                helperText={touched.mondayBatch ? errors.mondayBatch : ""}
                                error={touched.mondayBatch && Boolean(errors.mondayBatch)}
                                margin="dense"
                                variant="outlined"
                                fullWidth
                                
                                >
                                {classTiming.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                    </MenuItem>
                                ))}
                                </TextField>
                                </Grid>
                                </Grid>
                                {/* end */}
                                {/* Tuesday Start */}
                                <Grid item xs={12}>
                                        <Grid item xs={6}>
                                            <label id="switch-list-label-tuesday">Tuesday</label>
                                            <Switch
                                            edge="end"
                                            onChange={handleToggle('tuesday')}
                                            checked={checked.indexOf('tuesday') !== -1}
                                            imputProps={{ 'area-labelledby': 'switch-list-label-tuesday'}}
                                            />Is Weekoff?
                                        </Grid>
                                        <Grid item xs={12}>
                                    <TextField
                                        select
                                    id="tuesdayBatch"
                                    label="Class Timing"
                                    value={values.tuesdayBatch}
                                    onChange={handleChange("tuesdayBatch")}
                                    onBlur={handleBlur}
                                    helperText={touched.tuesdayBatch ? errors.tuesdayBatch : ""}
                                    error={touched.tuesdayBatch && Boolean(errors.tuesdayBatch)}
                                    margin="dense"
                                    variant="outlined"
                                    fullWidth
                                    
                                    >
                                    {classTiming.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                        </MenuItem>
                                    ))}
                                    </TextField>
                                    </Grid>
                                    </Grid>
                                    {/* end */}
                                    {/* Wednesday Start */}
                                    <Grid item xs={12}>
                                        <Grid item xs={6}>
                                            <label id="switch-list-label-wednesday">Wednesday</label>
                                            <Switch
                                            edge="end"
                                            onChange={handleToggle('wednesday')}
                                            checked={checked.indexOf('wednesday') !== -1}
                                            imputProps={{ 'area-labelledby': 'switch-list-label-wednesday'}}
                                            />Is Weekoff?
                                        </Grid>
                                        <Grid item xs={12}>
                                    <TextField
                                        select
                                        id="wednesdayBatch"
                                        label="Class Timing"
                                        value={values.wednesdayBatch}
                                        onChange={handleChange("wednesdayBatch")}
                                        onBlur={handleBlur}
                                        helperText={touched.wednesdayBatch ? errors.wednesdayBatch : ""}
                                        error={touched.wednesdayBatch && Boolean(errors.wednesdayBatch)}
                                        margin="dense"
                                        variant="outlined"
                                        fullWidth
                                        >
                                        {classTiming.map(option => (
                                            <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    </Grid>
                                </Grid> 
                            </Grid>     
                                {/* end */}
                            {/* Right Grid */}
                            <Grid item xs={6}>
                                {/* Thursday Start */}
                            <Grid item xs={12}>
                                    <Grid item xs={6}>
                                        <label id="switch-list-label-thursday">Thursday</label>
                                        <Switch
                                        edge="end"
                                        onChange={handleToggle('thursday')}
                                        checked={checked.indexOf('thursday') !== -1}
                                        imputProps={{ 'area-labelledby': 'switch-list-label-thursday'}}
                                        />Is Weekoff?
                                    </Grid>

                                    <Grid item xs={12}>
                                <TextField
                                    select
                                    id="thursdayBatch"
                                    label="Class Timing"
                                    value={values.thursdayBatch}
                                    onChange={handleChange("thursdayBatch")}
                                    onBlur={handleBlur}
                                    helperText={touched.thursdayBatch ? errors.thursdayBatch : ""}
                                    error={touched.thursdayBatch && Boolean(errors.thursdayBatch)}
                                    margin="dense"
                                    variant="outlined"
                                    fullWidth
                                    
                                    >
                                    {classTiming.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                </Grid>
                                </Grid>
                                {/* end */}
                                {/* Friday Start */}
                                <Grid item xs={12}>
                                    <Grid item xs={6}>
                                        <label id="switch-list-label-friday">Friday</label>
                                        <Switch
                                        edge="end"
                                        onChange={handleToggle('friday')}
                                        checked={checked.indexOf('friday') !== -1}
                                        imputProps={{ 'area-labelledby': 'switch-list-label-friday'}}
                                        />Is Weekoff?
                                    </Grid>
                                    <Grid item xs={12}>
                                <TextField
                                    select
                                    id="fridayBatch"
                                    label="Class Timing"
                                    value={values.fridayBatch}
                                    onChange={handleChange("fridayBatch")}
                                    onBlur={handleBlur}
                                    helperText={touched.fridayBatch ? errors.fridayBatch : ""}
                                    error={touched.fridayBatch && Boolean(errors.fridayBatch)}
                                    margin="dense"
                                    variant="outlined"
                                    fullWidth
                                    
                                    >
                                    {classTiming.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                </Grid>
                        </Grid>
                        {/* end */}
                        {/* Saturday Start */}
                        <Grid item xs={12}>
                                    <Grid item xs={6}>
                                        <label id="switch-list-label-saturday">Saturday</label>
                                        <Switch
                                        edge="end"
                                        onChange={handleToggle('saturday')}
                                        checked={checked.indexOf('saturday') !== -1}
                                        imputProps={{ 'area-labelledby': 'switch-list-label-saturday'}}
                                        />
                                        Is Weekoff?
                                    </Grid>
                                    <Grid item xs={12}>
                                <TextField
                                    select
                                    id="saturdayBatch"
                                    label="Class Timing"
                                    value={values.saturdayBatch}
                                    onChange={handleChange("saturdayBatch")}
                                    onBlur={handleBlur}
                                    helperText={touched.saturdayBatch ? errors.saturdayBatch : ""}
                                    error={touched.saturdayBatch && Boolean(errors.saturdayBatch)}
                                    margin="dense"
                                    variant="outlined"
                                    fullWidth
                                    
                                    >
                                    {classTiming.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                        </Grid>
                        </Grid>
                    {/* end */}
                    </Grid>
                </Grid>
                </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    batch,
    description,
    dateEffective,
    saturdayBatch,
    sundayBatch,
    mondayBatch,
    tuesdayBatch,
    wednesdayBatch,
    thursdayBatch,
    fridayBatch

    
  }) => {
    return {
      batch: batch || "",
      description: description || "",
      dateEffective: dateEffective || "",
      saturdayBatch: saturdayBatch || "",
      sundayBatch: sundayBatch || "",
      mondayBatch: mondayBatch || "",
      tuesdayBatch: tuesdayBatch || "",
      wednesdayBatch: wednesdayBatch || "",
      thursdayBatch: thursdayBatch || "",
      fridayBatch: fridayBatch || "",

    };
  },

  validationSchema: Yup.object().shape({
    batch: Yup.string().required("batch name is required"),
    description: Yup.string().required(" description required"),
    dateEffective: Yup.date().required("start date required"),
    saturdayBatch: Yup.string().required("classes timing required"),
    sundayBatch: Yup.string().required("classes timing required"),
    mondayBatch: Yup.string().required("classes timing required"),
    tuesdayBatch: Yup.string().required("classes timing required"),
    wednesdayBatch: Yup.string().required("classes timing required"),
    thursdayBatch: Yup.string().required("classes timing required"),
    fridayBatch: Yup.string().required("classes timing required"),

  }),


  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
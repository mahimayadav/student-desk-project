import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import AddNewTimeTable from './buttons/AddNewTimeTable';
import { Link } from 'react-router-dom';
const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  }
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
class AdvFilter extends React.Component {
  state = {
    columns: [
      {
        name: 'Batch',
        options: {
          filter: true
        }
      },
      {
        name: 'Date Effective',
        options: {
          filter: true,
        }
      },
      {
        name: 'Status',
        options: {
          filter: true,
          customBodyRender: (value) => {
            if (value === 'active') {
              return (<Chip label="Allocated" color="primary" />);
            }
            if (value === 'non-active') {
              return (<Chip label="Non Allocated" color="secondary" />);
            }
            return (<Chip label="Partially Allocated" />);
          }
        }
      },
      {
        name: 'Descriptions',
        options: {
          filter: true,
          
        }
      },
      {
        name: 'Created at',
        options: {
            filter: true,
        }
      },
      {
        name: 'Last Updated at',
        options: {
            filter: true,
        }
      },
      {
        name: 'Action',
        options: {
          filter: true,
           filter: false,
          sort: false,
          empty: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
             
                <Link to="/app/academic/timetable/allocate">
                  <button color="primary">
                      Allocation
                  </button>
                </Link>
            );
          }
        }
        
      }
    ],
    data: [
      ['second Class', '12/1/2020', 'active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '14/1/2020', 'non-active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '14/1/2020', ' active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '12/1/2020', 'non-active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '1/1/2020', ' active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '1/1/2020', 'non-active', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '1/1/2020', 'Patially Allocated', 'started soon', '2/3/2020','5/2/2020'],
      ['second Class', '1/1/2020', ' active', 'started soon', '2/3/2020','5/2/2020'],

    ]
  }

  render() {
    
    const { columns, data } = this.state;
    const { classes } = this.props;
    const options = {
      filterType: 'dropdown',
      responsive: 'stacked',
      print: false,
      rowsPerPage: 10,
      page: 0,
      customToolbar: () => {
        return (
          <AddNewTimeTable />
        );
      }

    };
    return (
      <div className={classes.table}>
        <MUIDataTable
          title="TimeTable"
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

AdvFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdvFilter);

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import AddSessionbutton from './buttons/AddSessionbutton';
import { Link } from 'react-router-dom';
const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  }
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
class AdvFilter extends React.Component {
  state = {
    columns: [
      {
        name: 'Name',
        options: {
          filter: true
        }
      },
      {
        name: 'Start Date',
        options: {
          filter: true,
        }
      },
      {
        name: 'End Date',
        options: {
          filter: false,
        }
      },
      {
        name: 'Status',
        options: {
          filter: true,
          customBodyRender: (value) => {
            if (value === 'active') {
              return (<Chip label="Active" color="primary" />);
            }
            if (value === 'non-active') {
              return (<Chip label="Non Active" color="secondary" />);
            }
            return (<Chip label="Unknown" />);
          }
        }
      },
      {
        name: 'Descriptions',
        options: {
          filter: true,
          
        }
      },
      {
        name: 'Action',
        options: {
          filter: true,
           filter: false,
          sort: false,
          empty: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
             
                <Link to="/app/academic/edit-session">
                  <button color="primary">
                      Edit
                  </button>
                </Link>
            );
          }
        }
        
      }
    ],
    data: [
      ['Gabby George', '1/1/2020', '1/9/2020', 'active', 'new sessions'],
      ['Aiden Lloyd', '1/1/2020', '1/6/2020', 'active', 'new sessions'],
      ['Jaden Collins', '1/1/2020', '1/6/2020', 'non-active', 'new sessions'],
      ['Franky Rees', '1/1/2020', '1/8/2020', 'active', 'new sessions'],
      ['Aaren Rose', '1/1/2020', '1/5/2020', 'unknown', 'new sessions'],
      ['Blake Duncan', '1/1/2020', '1/8/2020', 'active', 'new sessions'],
      ['Frankie Parry', '1/1/2020', '1/9/2020', 'non-active', 'new sessions'],
      ['Lane Wilson', '1/1/2020', '1/1/2020', 'active', 'new sessions'],
      ['Robin Duncan', '1/1/2020', '1/1/2020', 'unknown', 'new sessions'],
      ['Mel Brooks', '1/1/2020', '1/1/2020', 'active', 'new sessions'],
      ['Harper White', '1/1/2020', '1/1/2020', 'non-active', 'new sessions'],
      ['Kris Humphrey', '1/1/2020', '1/1/2020', 'active', 'new sessions'],
      ['Frankie Long', '1/1/2020', '1/1/2020', 'active', 'new sessions'],
      ['Brynn Robbins', '1/1/2020', '1/1/2020', 'active', 'new sessions'],
      ['Justice Mann', '1/1/2020', '1/1/2020', 'non-active', 'new sessions'],
      ['Addison Navarro', '1/1/2020', '1/1/2020', 'non-active', 'new sessions']
     
    ]
  }

  render() {
    
    const { columns, data } = this.state;
    const { classes } = this.props;
    const options = {
      filterType: 'dropdown',
      responsive: 'stacked',
      print: false,
      rowsPerPage: 10,
      page: 0,
      customToolbar: () => {
        return (
          <AddSessionbutton />
        );
      }

    };
    return (
      <div className={classes.table}>
        <MUIDataTable
          title="Sessions list"
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

AdvFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdvFilter);

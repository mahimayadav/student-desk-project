import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';


const defaultToolbarStyles = {
  iconButton: {
  },
};

class CustomToolbar extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip title={"add session"}>
        <Link className={classes.iconButton} to="/app/academic/add-session">
            <AddIcon className={classes.AddIcon} />
          </Link>
        </Tooltip>
        
      </React.Fragment>
    );
  }

}

export default withStyles(defaultToolbarStyles, { name: "CustomToolbar" })(CustomToolbar);
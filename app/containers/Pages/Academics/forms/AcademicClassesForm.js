import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CrudTable, Notification } from 'enl-components';
import styles from 'enl-components/Tables/tableStyle-jss';
import {
  fetchAction,
  addAction,
  removeAction,
  updateAction,
  editAction,
  saveAction,
  closeNotifAction,
} from '../reducers/crudTbActions';

// Reducer Branch
const branch = 'crudTableDemo';

const anchorTable = [
  {
    name: 'id',
    label: 'Id',
    type: 'static',
    initialValue: '',
    hidden: true
  }, {
    name: 'name',
    label: 'Name',
    type: 'text',
    initialValue: '',
    width: 'auto',
    hidden: false
  },{
    name: 'description',
    label: 'Description',
    type: 'textarea',
    initialValue: '',
    width: 'auto',
    hidden: false
  },{
    name: 'section',
    label: 'Sections',
    type: 'selection',
    initialValue: 'Section A',
    options: ['Section A', 'Section B', 'Section C', 'Section D'],
    width: 'auto',
    hidden: false
  }, 
   {
    name: 'edited',
    label: '',
    type: 'static',
    initialValue: '',
    hidden: true
  }, {
    name: 'action',
    label: 'Action',
    type: 'static',
    initialValue: '',
    hidden: false
  },
];
const dataApi = [
  {
    id: 1,
    name: '7th Standard',
    description:'max 50 student',
    section: 'Section A',
    edited: false,
    child: [
      {
        id: '1_1',
        name:'7th A',
        description: 'their is total 50 student',
      }
    ]
  }, {
    id: 2,
    name: '6th Standard',
    description:'max 50 student',
    section: 'Section B',
    edited: false,
    child: [
      {
        id: '2_1',
        name:'6th A',
        description: 'their is total 50 student',
      }
    ]
  }, {
    id: 3,
    name: '5th Standard',
    description:'max 50 student',
    section: 'Section A',
    edited: false,
    child: [
      {
        id: '3_1',
        name:'5th A',
        description: 'their is total 50 student',
      }
    ]
  }, {
    id: 4,
    name: '4th Standard',
    description:'max 50 student',
    section: 'Section A',
    edited: false,
  }, {
    id: 5,
    name: '9th Standard',
    description:'max 50 student',
    section: 'Section c',
    edited: false,
  }, {
    id: 6,
    name: '7th Standard',
    description:'max 50 student',
    section: 'Section B',
    edited: false,
  }
];

class CrudTableDemo extends Component {
  render() {
    const {
      classes,
      fetchData,
      addEmptyRow,
      dataTable,
      removeRow,
      updateRow,
      editRow,
      finishEditRow,
      closeNotif,
      messageNotif,
    } = this.props;
    return (
      <div>
        <Notification close={() => closeNotif(branch)} message={messageNotif} />
        <div className={classes.rootTable}>
          <CrudTable
            dataInit={dataApi}
            anchor={anchorTable}
            title="Academic Classes"
            dataTable={dataTable}
            fetchData={fetchData}
            addEmptyRow={addEmptyRow}
            removeRow={removeRow}
            updateRow={updateRow}
            editRow={editRow}
            finishEditRow={finishEditRow}
            branch={branch}
          />
        </div>
      </div>
    );
  }
}

CrudTableDemo.propTypes = {
  classes: PropTypes.object.isRequired,
  fetchData: PropTypes.func.isRequired,
  dataTable: PropTypes.object.isRequired,
  addEmptyRow: PropTypes.func.isRequired,
  removeRow: PropTypes.func.isRequired,
  updateRow: PropTypes.func.isRequired,
  editRow: PropTypes.func.isRequired,
  finishEditRow: PropTypes.func.isRequired,
  closeNotif: PropTypes.func.isRequired,
  messageNotif: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  force: state, // force state from reducer
  dataTable: state.getIn([branch, 'dataTable']),
  messageNotif: state.getIn([branch, 'notifMsg']),
});

const mapDispatchToProps = dispatch => ({
  fetchData: bindActionCreators(fetchAction, dispatch),
  addEmptyRow: bindActionCreators(addAction, dispatch),
  removeRow: bindActionCreators(removeAction, dispatch),
  updateRow: bindActionCreators(updateAction, dispatch),
  editRow: bindActionCreators(editAction, dispatch),
  finishEditRow: bindActionCreators(saveAction, dispatch),
  closeNotif: bindActionCreators(closeNotifAction, dispatch),
});

const CrudTableMapped = connect(
  mapStateToProps,
  mapDispatchToProps
)(CrudTableDemo);

export default withStyles(styles)(CrudTableMapped);

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { Field, reduxForm } from 'redux-form/immutable';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import {
  TextFieldRedux,
  SelectRedux,
} from 'enl-components/Authentication/ReduxFormMUI';
import { initAction, clearAction } from 'enl-redux/actions/reduxFormActions';

// validation functions
const required = value => (value == null ? 'Required' : undefined);

const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: 30
  },
  field: {
    width: '100%',
    marginBottom: 20
  },
  fieldBasic: {
    width: '100%',
    marginBottom: 20,
    marginTop: 10
  },
  inlineWrap: {
    display: 'flex',
    flexDirection: 'row'
  },
  buttonInit: {
    margin: theme.spacing(4),
    textAlign: 'center'
  },

});

const initData = {
  text: 'Sample Text',
  textarea: 'This is default text',
  selection: 'option2'
};

class AddClass extends React.Component {
  
    
    render() {
        const trueBool = true;
        const {
          classes,
          handleSubmit,
          pristine,
          reset,
          submitting,
          init,
          clear
        } = this.props;
        return (
          <div>
            <Grid container spacing={3} alignItems="flex-start" direction="row" justify="center">
              <Grid item xs={12} md={6}>
                <Paper className={classes.root}>
                  <div className={classes.buttonInit}>
                    <Button onClick={() => init(initData)} color="secondary" type="button">
                      Load Sample Data
                    </Button>
                    <Button onClick={() => clear()} type="button">
                      Clear Data
                    </Button>
                  </div>
                  <form onSubmit={handleSubmit}>
                    <div>
                      <Field
                        name="text"
                        component={TextFieldRedux}
                        placeholder="Class Name"
                        label="Class Name"
                        validate={required}
                        required
                        ref={this.saveRef}
                        className={classes.field}
                      />
                    </div>
                  
                    <div className={classes.field}>
                      <Field
                        name="textarea"
                        className={classes.field}
                        component={TextFieldRedux}
                        placeholder="Description"
                        label="Description"
                        multiline={trueBool}
                        rows={4}
                      />
                    </div>
                    <div>
                  <FormControl className={classes.field}>
                    <InputLabel htmlFor="selection">Select Section</InputLabel>
                    <Field
                      required
                      name="selection"
                      component={SelectRedux}
                      placeholder="Selection"
                      autoWidth={trueBool}
                    >
                      <MenuItem value="option1">Section A</MenuItem>
                      <MenuItem value="option2">Section B</MenuItem>
                      <MenuItem value="option3">Section C</MenuItem>
                    </Field>
                  </FormControl>
                </div>
                    <div>
                      <Button variant="contained" color="secondary" type="submit" disabled={submitting}>
                        Submit
                      </Button>
                      <Button
                        type="button"
                        disabled={pristine || submitting}
                        onClick={reset}
                      >
                        Reset
                      </Button>
                    </div>
                  </form>
                </Paper>
              </Grid>
            </Grid>
          </div>
        );
      }
    }
    
    AddClass.propTypes = {
      classes: PropTypes.object.isRequired,
      handleSubmit: PropTypes.func.isRequired,
      reset: PropTypes.func.isRequired,
      pristine: PropTypes.bool.isRequired,
      submitting: PropTypes.bool.isRequired,
      init: PropTypes.func.isRequired,
      clear: PropTypes.func.isRequired,
    };
    
    const mapDispatchToProps = dispatch => ({
      init: bindActionCreators(initAction, dispatch),
      clear: () => dispatch(clearAction),
    });
    
    const reduxFormMapped = reduxForm({
      form: 'immutableExample',
      enableReinitialize: true,
    })(AddClass);
    
    const reducer = 'initval';
    const FormInit = connect(
      state => ({
        force: state,
        initialValues: state.getIn([reducer, 'formValues'])
      }),
      mapDispatchToProps,
    )(reduxFormMapped);

export default withStyles(styles)(FormInit);
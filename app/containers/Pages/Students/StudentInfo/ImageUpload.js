import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'enl-api/dummy/brand';
import { SourceReader } from 'enl-components';
import { injectIntl, intlShape } from 'react-intl';
import { UploadInputImage } from '../../../../components/Forms/UploadInputImage';


class Upload extends React.Component {
  render() {
    const title = brand.name + ' - student';
    const description = brand.desc;
    const docSrc = 'components/Forms/';
    const {classes, intl } = this.props;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
          <div className={classes.root}>
            <UploadInputImage />
            <SourceReader componentName={docSrc + 'UploadInputImage.js'} />
          </div>
      </div>
    );
  }
}

Upload.propTypes = {
  classes: PropTypes.object.isRequired,
  intl: intlShape.isRequired
};

export default injectIntl(Upload);

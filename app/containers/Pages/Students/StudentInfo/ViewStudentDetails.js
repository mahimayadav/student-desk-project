import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import BasicInfo from '../../../../components/Forms/BasicInfo';
import ContactInfo from '../../../../components/Forms/ContactInfo';
import ParentsInfo from '../../../../components/Forms/ParentsInfo';
import DocumentInfo from '../../../../components/Forms/DocumentInfo';
import AttendenceHistory from '../../../../components/Forms/AttendenceHistory';
import QualificationInfo from '../../../../components/Forms/QualificationInfo';
import SiblingInfo from '../../../../components/Forms/SiblingInfo';
import FeeHistory from '../../../../components/Forms/FeeHistory';
import ExamReport from '../../../../components/Forms/ExamReport';
import UserLogin from '../../../../components/Forms/UserLogin';
import TerminationHistory from '../../../../components/Forms/TerminationHistory';
import AccountInfo from '../../../../components/Forms/AccountInfo';
import PromotionHistory from '../../../../components/Forms/PromotionHistory';
import Grid from '@material-ui/core/Grid';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import ViewRegistrationDetails from '../forms/ViewRegistrationDetails';


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    fontWeight: theme.typography.fontWeightMedium,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
  },
});

class ControlledAccordion extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;

    return (

      <div className={classes.root}>
          <Grid container >
           <Grid xs={4}>
        <Card>
            <CardContent>
               
                    <List>
                            <ListItem>Pics</ListItem>
                            <ListItem>
                                
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                        
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                        
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>
                            
                            <ListItem>
                            
                            </ListItem>


                    </List>
                
            </CardContent>
        </Card>
        </Grid>
        <Grid xs={8}>
        <Card>
           <CardContent>
             <h1>Personal Information</h1>
                
            
            </CardContent> 
            </Card>
        </Grid>
        </Grid>
      </div>
    );
  }
}

ControlledAccordion.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledAccordion);


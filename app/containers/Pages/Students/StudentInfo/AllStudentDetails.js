import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import brand from 'enl-api/dummy/brand';
import { SourceReader } from 'enl-components';
import { injectIntl, intlShape } from 'react-intl';
import AllStudentList from '../forms/AllStudentList'

class StudentList extends Component {
  render() {
    const title = brand.name + ' - Students';
    const description = brand.desc;
    const docSrc = 'containers/Students/forms/';
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
       
          <div>
            <AllStudentList />
            <SourceReader componentName={docSrc + 'AdvFilter.js'} />
          </div>

      </div>
    );
  }
}

StudentList.propTypes = {
  intl: intlShape.isRequired
};

export default injectIntl(StudentList);

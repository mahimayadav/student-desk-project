import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import imgApi from 'enl-api/images/photos';
import avatarApi from 'enl-api/images/avatars';
import { PostCard } from 'enl-components';


const styles = theme => ({
  root: {
    width: '100%',
    '& > *': {
      margin: theme.spacing(1),
    },
    
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    fontWeight: theme.typography.fontWeightSmall,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
  },
});

class ControlledAccordion extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
  

    return (

      <div className={classes.root}>
        <Grid container >
          <Grid xs={4}>
            <Card>
              <CardContent>
                <List>
                <ListItem>
                <PostCard
                  date="Sept, 15 2018"
                  content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sed urna in justo euismod condimentum."
                  image={imgApi[11]}
                  avatar={avatarApi[10]}
                  name="Jane Doe"
                />
                </ListItem>
                <ListItem>
                  <Button>Personal Information</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Contact Information</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Parent Information</Button>
                </ListItem>
                        
                <ListItem>
                  <Button>Document Information</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Qualification Information</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Sibling Information</Button>
                </ListItem>
                        
                <ListItem>
                  <Button>Account Information</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>User Login</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Promotion History</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Termination History</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Exam Report</Button>
                </ListItem>
                            
                <ListItem>
                  <Button>Fee History</Button>
                </ListItem>

                <ListItem>
                  <Button>Attendence Information</Button>
                </ListItem>            
                </List>
              </CardContent>
          </Card>
        </Grid>
        <Grid xs={8}>
        <Card>
           <CardContent>
             <h1>Personal Information</h1>
                
            
            </CardContent> 
            </Card>
        </Grid>
        </Grid>
      </div>
    );
  }
}

ControlledAccordion.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledAccordion);





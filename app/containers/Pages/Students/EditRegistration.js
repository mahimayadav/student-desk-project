import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";


const styles = () => ({
  card: {
    maxWidth: 420,
    marginTop: 50
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});

const sectionCategory = [
  {
    value: "sectionA",
    label: "Section A"
  },
  {
    value: "sectionB",
    label: "Section B"
  },
  {
    value: "sectionC",
    label: "Section C"
  }
];

const form = props => {
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <h1>Classes Form</h1>
            <TextField
              id="className"
              label="Class Name"
              value={values.className}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.className ? errors.className : ""}
              error={touched.className && Boolean(errors.className)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
             
            <TextField
              id="description"
              label="Description"
              value={values.description}
              onChange={handleChange}
              onBlur={handleBlur}
              multiline={trueBool}
              rows={4}
              helperText={touched.description ? errors.description : ""}
              error={touched.description && Boolean(errors.description)}
              margin="dense"
              variant="outlined"
              fullWidth
            />
               <TextField
              select
              id="section"
              label="Sections"
              value={values.section}
              onChange={handleChange("section")}
              helperText={touched.section ? errors.section : ""}
              error={touched.section && Boolean(errors.section)}
              margin="dense"
              variant="outlined"
              fullWidth
            >
              {sectionCategory.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    className,
    description,
    section
  }) => {
    return {
      className: className || "",
      description: description || "",
      section: section || "",
     
    };
  },

  validationSchema: Yup.object().shape({
    className: Yup.string().required("class name is required"),
    description: Yup.string().required("description required"),
    section: Yup.string().required("Select your section category"),
   
  }),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const styles = () => ({
  card: {
    maxWidth: 420,
    marginTop: 50
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});


const form = props => {
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <h1>Previous School Details Form</h1>
            <label>Institute Name</label>
            <TextField
              id="previousInstituteName"
              placeholder="enter previous institute name"
              value={values.previousInstituteName}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.previousInstituteName ? errors.previousInstituteName : ""}
              error={touched.previousInstituteName && Boolean(errors.previousInstituteName)}
              margin="dense"
              variant="outlined"
              fullWidth
              
            />
            <label>Remarks</label>
             <TextField
              id="remark"
              placeholder="enter description"
              value={values.remark}
              onChange={handleChange}
              onBlur={handleBlur}
              margin="dense"
              variant="outlined"
              fullWidth
              multiline={trueBool}
              rows={3}
            /> 
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    previousInstituteName,
    remark
  }) => {
    return {
        previousInstituteName: previousInstituteName || "",
        remark: remark || "",
    };
  },

  validationSchema: Yup.object().shape({
    previousInstituteName: Yup.string().required("class name is required"),
    
   
  }),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
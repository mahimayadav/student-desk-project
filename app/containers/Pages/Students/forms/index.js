
// Student Registration List
export StudentRegistrationList from './StudentRegistrationList';
export PreviousSchoolRegistration from './PreviousSchoolRegistration';
export ParentsRegistration from './ParentsRegistration';
export StudentRegistration from './StudentRegistration';

//Upload Image
// export UploadInputimage from './UploadInputImage';
// export UploadInputAll from './UploadInputAll';
// export UploadInputButton from './UploadInputButton';



export ViewRegistrationDetails from './ViewRegistrationDetails';
export UploadStudentFile from './UploadStudentFile';
export AllStudentList from './AllStudentList';



import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import AddIcon from "@material-ui/icons/Add";
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const defaultToolbarStyles = {
  iconButton: {
  },
};
const styles = theme => ({
    demo: {
      height: 'auto',
    },
    divider: {
      display: 'block',
      margin: `${theme.spacing(3)}px 0`,
    },
    field: {
      margin: `${theme.spacing(3)}px 0`,
    },
    button: {
      margin: theme.spacing(1),
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    margin: {
      margin: theme.spacing(1),
    },
    cssRoot: {
      color: theme.palette.getContrastText(purple[500]),
      backgroundColor: purple[500],
      '&:hover': {
        backgroundColor: purple[700],
      },
    },
    bootstrapRoot: {
      boxShadow: 'none',
      textTransform: 'none',
      borderRadius: 4,
      fontSize: 16,
      padding: '6px 12px',
      border: '1px solid',
      backgroundColor: '#007bff',
      borderColor: '#007bff',
      '&:hover': {
        backgroundColor: '#0069d9',
        borderColor: '#0062cc',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      },
    },
    gradientBtn: {
      background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
      borderRadius: 3,
      border: 0,
      color: 'white',
      height: 48,
      padding: '0 30px',
      boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
    },
    label: {
      textTransform: 'capitalize',
    },
    inputUpload: {
      display: 'none',
    },
  });
  
  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });
class CustomToolbar extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip title={"view details"}>
        <Link className={classes.iconButton} to="/app/student/view-details">
   
                
                <Button
                   variant="contained"
                   color="primary"
                   disableRipple
                  className={classNames(classes.margin, classes.bootstrapRoot)}
                >
                  View
                </Button>
                
               
          </Link>
        </Tooltip>
        
      </React.Fragment>
    );
  }

}

export default withStyles(defaultToolbarStyles, { name: "CustomToolbar" })(CustomToolbar);
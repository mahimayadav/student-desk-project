import React from "react";
import Tooltip from "@material-ui/core/Tooltip";
import EditIcon from "@material-ui/icons/Edit";
import { withStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom';


const defaultToolbarStyles = {
  iconButton: {
  },
};

class CustomToolbar extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Tooltip title={"edit details"}>
        <Link className={classes.iconButton} to="/app/student/edit-registration">
            <EditIcon className={classes.EditIcon} />
          </Link>
        </Tooltip>
        
      </React.Fragment>
    );
  }

}

export default withStyles(defaultToolbarStyles, { name: "CustomToolbar" })(CustomToolbar);
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
// import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import AddNewStudent from './buttons/AddNewStudent';
import ViewDetails from './buttons/ViewDetails';


const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  }
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
class AdvFilter extends React.Component {
  state = {
    columns: [
      {
        name: 'Adm No',
        options: {
          filter: true
        }
      },
      {
          name: 'First Name',
          options: {
              filter: true,
          }
      },
      {
        name: 'Last Name',
        options: {
            filter: true,
        }
      },
      {
        name: 'Father Name',
        options: {
            filter: true,
        }
      },
      
      {
        name: 'Date of Admission',
        options: {
          filter: false,
        }
      },
      {
        name: 'Contact',
        options: {
          filter: true,
          
        }
      },
      {
        name: 'Course',
        options: {
            filter: true,
        }
      },
      {
        name: 'Batch',
        options: {
            filter: true,
        }
      },
      {
        name: 'Action',
        options: {
          filter: true,
           filter: false,
          sort: false,
          empty: true,
          customBodyRender: () => {
            return (
                 <ViewDetails />
                
            );
          }
        }
        
      }
    ],
    data: [
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],
      ['s123','Gabby',' George', 'hekum', '1/1/2020', '980459598', 'lkg', 'sectionA'],

      
     
    ]
  }

  render() {
    
    const { columns, data } = this.state;
    const { classes } = this.props;
    const options = {
      filterType: 'dropdown',
      responsive: 'stacked',
      print: false,
      rowsPerPage: 10,
      page: 0,
      customToolbar: () => {
        return (
          <AddNewStudent />
        );
      }

    };
    return (
      <div className={classes.table}>
        <MUIDataTable
          title="Students list"
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

AdvFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdvFilter);

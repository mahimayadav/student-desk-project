import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import AddNewStudent from './buttons/AddNewStudent';

import ViewDetails from './buttons/ViewDetails';

const styles = theme => ({
  // cssRoot: {
  //   color: theme.palette.getContrastText(purple[500]),
  //   backgroundColor: purple[500],
  //   '&:hover': {
  //     backgroundColor: purple[700],
  //   },
  // },
  // bootstrapRoot: {
  //   boxShadow: 'none',
  //   textTransform: 'none',
  //   borderRadius: 4,
  //   fontSize: 16,
  //   padding: '6px 12px',
  //   border: '1px solid',
  //   backgroundColor: '#007bff',
  //   borderColor: '#007bff',
  //   '&:hover': {
  //     backgroundColor: '#0069d9',
  //     borderColor: '#0062cc',
  //   },
  //   '&:active': {
  //     boxShadow: 'none',
  //     backgroundColor: '#0062cc',
  //     borderColor: '#005cbf',
  //   },
  //   '&:focus': {
  //     boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
  //   },
  // },
  // gradientBtn: {
  //   background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  //   borderRadius: 3,
  //   border: 0,
  //   color: 'white',
  //   height: 48,
  //   padding: '0 30px',
  //   boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .30)',
  // },
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  }
});
// const theme = createMuiTheme({
//   palette: {
//     primary: green,
//   },
// });
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
class AdvFilter extends React.Component {
  
  state = {
    columns: [
      {
        name: 'Name',
        options: {
          filter: true
        }
      },
      {
        name: 'Gaurdian Name',
        options: {
          filter: true,
        }
      },
      {
        name: ' Date of Birth',
        options: {
          filter: false,
        }
      },
      {
        name: 'Contact No',
        options: {
          filter: false,
        }
      },
      {
        name: 'Course',
        options: {
          filter: false,
        }
      },
      {
        name: 'Status',
        options: {
          filter: true,
          customBodyRender: (value) => {
            if (value === 'active') {
              return (<Chip label="Active" color="primary" />);
            }
            if (value === 'non-active') {
              return (<Chip label="Non Active" color="secondary" />);
            }
            return (<Chip label="Unknown" />);
          }
        }
      },
      {
        name: 'Date Of Registration',
        options: {
          filter: true,
          
        }
      },
      {
        name: 'Registration fee',
        options: {
          filter: true,
          
        }
      },
      {
        name: 'Action',
        options: {
          filter: true,
           filter: false,
          sort: false,
          empty: true,
          customBodyRender: () => {
            return (
             <ViewDetails />
            );
          }
        }
        
      }
    ],
    data: [
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','non-active', '1/9/2020', '$500'],
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],

      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],
      ['Gabby George','heily', '1/1/2020', '9063456767','Lkg','active', '1/9/2020', '$500'],
    ]
  }

  render() {
    
    const { columns, data } = this.state;
    const { classes } = this.props;
    const options = {
      filterType: 'dropdown',
      responsive: 'stacked',
      print: false,
      rowsPerPage: 10,
      page: 0,
      customToolbar: () => {
        return (
          <AddNewStudent />
        );
      }

    };
    return (
      <div className={classes.table}>
        <MUIDataTable
          title="Registration list"
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

AdvFilter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdvFilter);

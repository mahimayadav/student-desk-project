import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'enl-api/dummy/brand';
import { SourceReader } from 'enl-components';
import { injectIntl, intlShape } from 'react-intl';
import { UploadInputAll } from '.';

class Upload extends React.Component {
  render() {
    const title = brand.name + ' - student';
    const description = brand.desc;
    const docSrc = 'containers/Students/forms/';
    const {classes } = this.props;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
          <div className={classes.root}>
            <UploadInputAll />
            <SourceReader componentName={docSrc + 'UploadInputAll.js'} />
          </div>
      </div>
    );
  }
}

Upload.propTypes = {
  classes: PropTypes.object.isRequired,
  intl: intlShape.isRequired
};

export default injectIntl(Upload);

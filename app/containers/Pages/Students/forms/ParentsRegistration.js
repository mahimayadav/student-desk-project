import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import MuiPhoneNumber from "material-ui-phone-number";
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const styles = () => ({
  card: {
    maxWidth: 800,
    marginTop: 20,
    marginleft: 40
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
});

const relationCategory = [
  {
    value: "father",
    label: "Father"
  },
  {
    value: "mother",
    label: "Mother"
  },
  {
    value: "other",
    label: "Other"
  }
];

  const occupationCategory = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];

  const noOfChild = [
    {
        value: "one",
        label: "One"
      },
      {
        value: "two",
        label: "Two"
      },
      {
        value: "three",
        label: "Three"
      } 

  ];

const form = props => {
    const [selectedValue, setSelectedValue] = React.useState('existingParents');
    const handleOptionChange = (event) => {
        setSelectedValue(event.target.value);
      };
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <h1>Parents Details</h1>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <label>
                    <Radio
                        checked={selectedValue === 'existingParents'}
                        onChange={handleOptionChange}
                        value="existingParents"
                        name="parents"
                        inputProps={{ 'aria-label': 'Existing Parents' }}
                    />
                        Existing Parents
                    </label>
                    <label>
                    <Radio
                        checked={selectedValue === 'newParents'}
                        onChange={handleOptionChange}
                        value="newParents"
                        name="parents"
                        inputProps={{ 'aria-label': 'New Parents' }}
                    />
                    New Parents
                    </label>
                </Grid>
            
                <Grid item xs={6}>
                    <label>First Gardian Name</label>
                    <TextField
                        id="firstGardianName"
                        placeholder="First Gardian Name"
                        value={values.firstGardianName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        helperText={touched.firstGardianName ? errors.firstGardianName : ""}
                        error={touched.firstGardianName && Boolean(errors.firstGardianName)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={6}>
                <label>Second Gardian Name</label>
                    <TextField
                        id="secondGardianName"
                        placeholder="Second Gardian Name"
                        alue={values.secondGardianName}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        helperText={touched.secondGardianName && errors.secondGardianName ? "has-error" : null}
                        error={touched.secondGardianName && Boolean(errors.secondGardianName)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    />
                </Grid>
                
                <Grid item xs={6}>
                    <label>First Gardian Relation</label>
                    <TextField
                        select
                        id="firstGardianrelation"
                        placeholder="Select Relation"
                        value={values.firstGardianrelation}
                        onChange={handleChange("firstGardianrelation")}
                        helperText={touched.firstGardianrelation ? errors.firstGardianrelation : ""}
                        error={touched.firstGardianrelation && Boolean(errors.firstGardianrelation)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                    
                    >
                        {relationCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={6}>
                    <label>Second Gardian Relation</label>
                    <TextField
                        select
                        id="secondGardianrelation"
                        placeholder="Select Relation"
                        value={values.secondGardianrelation}
                        onChange={handleChange("secondGardianrelation")}
                        helperText={touched.secondGardianrelation ? errors.secondGardianrelation : ""}
                        error={touched.secondGardianrelation && Boolean(errors.secondGardianrelation)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                        
                        >
                        {relationCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={6}>
                    <label>First Gardian Occupation</label>
                    <TextField
                        select
                        id="firstGardianOccupation"
                        placeholder="Occupation"
                        value={values.firstGardianOccupation}
                        onChange={handleChange("firstGardianOccupation")}
                        helperText={touched.firstGardianOccupation ? errors.firstGardianOccupation : ""}
                        error={touched.firstGardianOccupation && Boolean(errors.firstGardianOccupation)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                        
                        >
                        {occupationCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={6}>
                    <label>Second Gardian Occupation</label>
                    <TextField
                        select
                        id="secondGardianOccupation"
                        placeholder="Occupation"
                        value={values.secondGardianOccupation}
                        onChange={handleChange("secondGardianOccupation")}
                        helperText={touched.secondGardianOccupation ? errors.secondGardianOccupation : ""}
                        error={touched.secondGardianOccupation && Boolean(errors.secondGardianOccupation)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                        
                        >
                        {occupationCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
                <Grid item xs={6}>
                    <label>Contact Number:</label>
                    <MuiPhoneNumber 
                        id="parentContactNo1"
                        label="Phone Number"
                        defaultCountry={"us"}
                        variant="filled"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        fullWidth
                        value={values.parentContactNo1}
                        helperText={touched.parentContactNo1 && errors.parentContactNo1 ? "has-error" : null}
                        error={touched.parentContactNo1 && Boolean(errors.parentContactNo1)}
                    />
                </Grid>
                <Grid item xs={6}>
                    <label>Contact Number:</label>
                    <MuiPhoneNumber 
                        id="parentContactNo2"
                        label="Phone Number"
                        defaultCountry={"us"}
                        variant="filled"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.parentContactNo2}
                        helperText={touched.parentContactNo2 && errors.parentContactNo2 ? "has-error" : null}
                        error={touched.parentContactNo2 && Boolean(errors.parentContactNo2)}
                        fullWidth
                    />
                </Grid>
                    
                <Grid item xs={6}>
                    <label>Total No of Child</label>
                    <TextField
                        select
                        id="noOfChild"
                        value={values.noOfChild}
                        onChange={handleChange("noOfChild")}
                        helperText={touched.noOfChild ? errors.noOfChild : ""}
                        error={touched.noOfChild && Boolean(errors.noOfChild)}
                        margin="dense"
                        variant="outlined"
                        fullWidth
                        
                        >
                        {noOfChild.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
              
            </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    noOfChild,
    firstGardianName,
    secondGardianName,
    studentContactNo,
    parentContactNo1,
    parentContactNo2,
    firstGardianOccupation,
    secondGardianOccupation,
    firstGardianrelation,
    secondGardianrelation,
    parents
  }) => {
    return {
      noOfChild: noOfChild || "",
      firstGardianOccupation: firstGardianOccupation || "",
      secondGardianOccupation: secondGardianOccupation || "",
      studentContactNo: studentContactNo || "",
      parentContactNo1: parentContactNo1 || "", 
      parentContactNo2: parentContactNo2|| "",
      firstGardianName: firstGardianName || "",
      secondGardianName: secondGardianName || "",
      firstGardianrelation: firstGardianrelation || "",
      secondGardianrelation: secondGardianrelation || "",
      parents: parents || "",
     
    };
  },

  validationSchema: Yup.object().shape({
    parents: Yup.string().required("Select parents"),
    secondGardianName: Yup.string().required(" gardian name is required"),
    firstGardianrelation: Yup.string().required(" gardian relation is required"),
    previousInstituteName: Yup.string().required(" institute name is required"),
    gender: Yup.string().required(" gender is required"),
    firstGardianName: Yup.string().required(" gardian name is required"),
    secondGardianrelation: Yup.string().required(" gardian relation is required"),
    parentContactNo1: Yup.string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("Must enter a phone number"),
    parentContactNo2: Yup.string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("Must enter a phone number")
   
  }),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
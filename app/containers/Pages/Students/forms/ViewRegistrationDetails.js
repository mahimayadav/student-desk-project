import React from "react";
import { withFormik } from "formik";
import { withStyles } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

  

const styles = () => ({
  card: {
    maxWidth: 900,
    marginTop: 20
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
 
});



const form = props => {
   const trueBool = true;
  const {
    classes,
  } = props;

  return (
    <div className={classes.container}>
        <Card className={classes.card}>
          <CardContent>
          <List>
                        <ListItem>Pics</ListItem>
                        <Divider />
                        <ListItem>
                            <label>Name</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Father's Name</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Mother's Name</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Contact Number</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Gender</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Date of Birth</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Batch</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Date of Admission</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Admission Number</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Date of Promotion</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Created At</label>
                        </ListItem>
                        <Divider />
                        <ListItem>
                        <label>Last Updated date</label>
                        </ListItem>


                    </List>
          </CardContent>
         
        </Card>
    </div>
  );
};

const Form = withFormik({

  mapPropsToValues: ({
    firstName,
    midName,
    lastName,
    course,
    student,
    registrationDate,
    birthDate,
    gender


  }) => {
    return {
      firstName: firstName || "",
      midName: midName || "",
      lastName: lastName || "",
      course: course || "",
      student: student || "",
      registrationDate: registrationDate || "",
      birthDate: birthDate || "",
      gender: gender || "",
     
    };
  },

})(form);

export default withStyles(styles)(Form);
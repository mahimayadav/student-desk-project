import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import BasicInfo from '../../../../components/Forms/BasicInfo';
import ContactInfo from '../../../../components/Forms/ContactInfo';
import ParentsInfo from '../../../../components/Forms/ParentsInfo';
import DocumentInfo from '../../../../components/Forms/DocumentInfo';
import AttendenceHistory from '../../../../components/Forms/AttendenceHistory';
import QualificationInfo from '../../../../components/Forms/QualificationInfo';
import SiblingInfo from '../../../../components/Forms/SiblingInfo';
import FeeHistory from '../../../../components/Forms/FeeHistory';
import ExamReport from '../../../../components/Forms/ExamReport';
import UserLogin from '../../../../components/Forms/UserLogin';
import TerminationHistory from '../../../../components/Forms/TerminationHistory';
import AccountInfo from '../../../../components/Forms/AccountInfo';
import PromotionHistory from '../../../../components/Forms/PromotionHistory';
import Grid from '@material-ui/core/Grid';
// import ViewRegistrationDetails from '../forms/ViewRegistrationDetails';


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
    fontWeight: theme.typography.fontWeightMedium,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
  },
});

class ControlledAccordion extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
    const { expanded } = this.state;

    return (

      <div className={classes.root}>
         <Grid container >
             <Grid xs={8}>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                
                    <Typography className={classes.heading}>
                    Basic Information
                    </Typography>
                    {/* <Typography className={classes.secondaryHeading}>I am an expansion panel</Typography> */}
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <BasicInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Contact Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <ContactInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChange('panel3')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Parents Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <ParentsInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel4'} onChange={this.handleChange('panel4')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Documents Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <DocumentInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel5'} onChange={this.handleChange('panel5')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Qualification Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <QualificationInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel6'} onChange={this.handleChange('panel6')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Sibling Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <SiblingInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel7'} onChange={this.handleChange('panel7')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Account Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <AccountInfo />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel8'} onChange={this.handleChange('panel8')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>User Login</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <UserLogin />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel9'} onChange={this.handleChange('panel9')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Promotion History</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <PromotionHistory />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel10'} onChange={this.handleChange('panel10')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Termination History</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <TerminationHistory />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel11'} onChange={this.handleChange('panel11')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Exam Report</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <ExamReport />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel12'} onChange={this.handleChange('panel12')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Fee History</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <FeeHistory />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel13'} onChange={this.handleChange('panel13')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Attendence Information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography>
                    <AttendenceHistory />
                    </Typography>
                </ExpansionPanelDetails>
                </ExpansionPanel>
            </Grid> 
            <Grid xs={4}>
                {/* <ViewRegistrationDetails /> */}
            
            </Grid>
        </Grid>
      </div>
    );
  }
}

ControlledAccordion.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledAccordion);


import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import BasicInfo from '../../../../components/Forms/BasicInfo';
import ParentsInfo from '../../../../components/Forms/ParentsInfo';
import PromotionHistory from '../../../../components/Forms/PromotionHistory';
import DocumentInfo from '../../../../components/Forms/DocumentInfo';
import AttendenceHistory from '../../../../components/Forms/AttendenceHistory';
import QualificationInfo from '../../../../components/Forms/QualificationInfo';
import SiblingInfo from '../../../../components/Forms/SiblingInfo';
import FeeHistory from '../../../../components/Forms/FeeHistory';
import ExamReport from '../../../../components/Forms/ExamReport';
import UserLogin from '../../../../components/Forms/UserLogin';
import ContactInfo from '../../../../components/Forms/ContactInfo';
import TerminationHistory from '../../../../components/Forms/TerminationHistory';
import AccountInfo from '../../../../components/Forms/AccountInfo';

const styles = theme => ({
  root: {
    width: '90%',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
});

function getSteps() {
    return ['Basic Information', 'Contact Information', 'Parent Information', ' Document ','Qualification','Sibling','Account Information','User Login','Promotion History','Termination History','Exam Report', 'Fee History', 'Attendence '];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return <BasicInfo />;
    case 1:  
      return <ContactInfo />;
    case 2:
      return <ParentsInfo />;
    case 3:
      return <DocumentInfo /> ;
    case 4:
      return <QualificationInfo /> ;
    case 5:
      return <SiblingInfo /> ;
    case 6:
      return <AccountInfo /> ;
    case 7:
      return <UserLogin />;
    case 8:
      return <PromotionHistory /> ;  
    case 9:
      return <TerminationHistory /> ;
    case 10:
      return <ExamReport /> ;
    case 11:
      return <FeeHistory /> ;
    case 12:
      return <AttendenceHistory /> ;   
    default:
      return 'Unknown step';
  }
}

class VerticalStepper extends React.Component {
  state = {
    activeStep: 0,
  };

  handleNext = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep + 1,
    });
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep - 1,
    });
  };

  handleReset = () => {
    this.setState({
      activeStep: 0,
    });
  };

  render() {
    const { classes } = this.props;
    const steps = getSteps();
    const { activeStep } = this.state;

    return (
      <div className={classes.root}>
        <Stepper activeStep={activeStep} orientation="vertical">
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent>
                <Typography>{getStepContent(index)}</Typography>
                <div className={classes.actionsContainer}>
                  <div>
                    <Button
                      disabled={activeStep === 0}
                      onClick={this.handleBack}
                      className={classes.button}
                    >
                      Back
                    </Button>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={this.handleNext}
                      className={classes.button}
                    >
                      {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                    </Button>
                  </div>
                </div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&quot;re finished</Typography>
            <Button onClick={this.handleReset} className={classes.button}>
              Reset
            </Button>
          </Paper>
        )}
      </div>
    );
  }
}

VerticalStepper.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VerticalStepper);
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import brand from 'enl-api/dummy/brand';
import { PapperBlock } from 'enl-components';

class Reception extends Component {
  render() {
    const title = brand.name + ' - Timeline';
    const description = brand.desc;
    return (
      <div>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
        </Helmet>
        <PapperBlock title="WELCOME TO EDUMATIONS" icon="insert_chart" desc="" overflowX>
          <p>Reception</p>
        </PapperBlock>
      </div>
    );
  }
}

export default Reception;

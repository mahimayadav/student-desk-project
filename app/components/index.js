import { DatePicker } from "@material-ui/pickers";
import UploadInputImg from "./Forms/UploadInputImage";
import UploadInputAll from "./Forms/UploadInputAll";
import IdentityCard from "./Cards/IdentityCard";
import PostCard from "./Cards/PostCard";



//  Main Components
export Header from './MainComponents/Header/Header';
export HeaderMenu from './MainComponents/Header/HeaderMenu';
export Sidebar from './MainComponents/Sidebar';
export SidebarBig from './MainComponents/SidebarBig';
export BreadCrumb from './MainComponents/BreadCrumb/BreadCrumb';
export PapperBlock from './MainComponents/PapperBlock/PapperBlock';
export SearchUi from './MainComponents/Search/SearchUi';
export SelectLanguage from './MainComponents/SelectLanguage';
export SourceReader from './MainComponents/SourceReader/SourceReader';

// Guide
export GuideSlider from './Others/GuideSlider';

// Authentication Starts
export LoginForm from './Authentication/LoginForm';
export ResetForm from './Authentication/ResetForm';
export LockForm from './Authentication/LockForm';

// Authentication Starts

// Error
export ErrorWrap from './Others/Error/ErrorWrap';

//  Template Settings

export UIConfig from './UIConfig';


// Form
export Notification from './Notification/Notification';

// Table Components
export CrudTable from './Tables/CrudTable';

//Forms

export MaterialDropZone from './Forms/MaterialDropZone';

//Student Details
export BasicInfo from './Forms/BasicInfo';
export ContactInfo from './Forms/ContactInfo';
export ParentsInfo from './Forms/ParentsInfo';
export DocumentInfo from './Forms/DocumentInfo';
export PromotionHistory from './Forms/PromotionHistory';
export SiblingInfo from './Forms/SiblingInfo';
export TerminationHistory from './Forms/TerminationHistory';
export UserLogin from './Forms/UserLogin';
export QualificationInfo from './Forms/QualificationInfo';
export FeeHistory from './Forms/FeeHistory';
export ExamReport from './Forms/ExamReport';
export AccountInfo from './Forms/AccountInfo';
export AttendenceHistory from './Forms/AttendenceHistory';
export UploadInputImage from './Forms/UploadInputImage';
export UploadInputAll from './Forms/UploadInputAll';
export UploadInputButton from './Forms/UploadInputButton';
export IdentityCard from './Cards/ProfileCard';
export PostCard from './Cards/ProfileCard';
export ProfileCard from './Cards/ProfileCard';
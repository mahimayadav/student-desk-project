import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles, RadioGroup } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import Grid from '@material-ui/core/Grid';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
  } from "@material-ui/pickers";
  import DateFnsUtils from "@date-io/date-fns";
  import Radio from '@material-ui/core/Radio';
  

const styles = () => ({
  card: {
    maxWidth: 900,
    marginTop: 20
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
 
});

const religion = [
  {
    value: "father",
    label: "Father"
  },
  {
    value: "mother",
    label: "Mother"
  },
  {
    value: "other",
    label: "Other"
  }
];
const casteCategory = [
    {
      value: "lkg",
      label: "LKG"
    },
    {
      value: "ukg",
      label: "UKG"
    },
    {
      value: "nursery",
      label: "Nursery"
    }
  ];

  const caste = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];
  const bloodGroup = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];

  const nationality = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];
  const motherTongue = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];


const form = props => {
    const [selectedValue1, setSelectedValue1] = React.useState('male');
    const handleOptionChange1 = (event) => {
        setSelectedValue1(event.target.value);
      };
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <Grid container spacing={2}>
            <Grid item xs={4}> 
              <label>First Name</label> 
                        <TextField
                          id="firstName"
                          placeholder="First Name"
                          value={values.firstName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.firstName ? errors.firstName : ""}
                          error={touched.firstName && Boolean(errors.firstName)}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                      
                        />
              </Grid> 
              <Grid item xs={4}>
              <label>Middle Name</label>
                        <TextField
                          id="midName"
                          placeholder="Middle Name"
                          value={values.midName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                        
                        />
              </Grid>
              <Grid item xs={4}>
                <label>Last Name</label>
                        <TextField
                          id="lastName"
                          placeholder="Last Name"
                          value={values.lastName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.lastName ? errors.lastName : ""}
                          error={touched.lastName && Boolean(errors.lastName)}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                          
                          
                        />
              </Grid> 
              <Grid item xs={4}>
                <label>Date of Birth</label>
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              margin="dense"
                              variant="outlined"
                              fullWidth
                              id="birthDate"
                              placeholder="Date of Birth"
                              format="MM/dd/yyyy"
                              onBlur={handleBlur}
                              value={values.birthDate}
                              onChange={date => setFieldValue('birthDate', date)}
                              helperText={touched.birthDate ? errors.birthDate : ""}
                              error={touched.birthDate && Boolean(errors.birthDate)}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}

                        />
                        </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={4}>
                <label>Birth Place</label>
                        <TextField
                          id="lastName"
                          placeholder="Last Name"
                          value={values.lastName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.lastName ? errors.lastName : ""}
                          error={touched.lastName && Boolean(errors.lastName)}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                          
                          
                        />
              </Grid>
              <Grid item xs={4}> 
                        <h3>Gender</h3>
                        <label>
                        <Radio
                            checked={selectedValue1 === 'male'}
                            onChange={handleOptionChange1}
                            value="male"
                            name="gender"
                            inputProps={{ 'aria-label': 'Male' }}
                        />
                            Male
                        </label>
                        <label>
                        <Radio
                            checked={selectedValue1 === 'female'}
                            onChange={handleOptionChange1}
                            value="female"
                            name="gender"
                            inputProps={{ 'aria-label': 'Female' }}
                        />
                          Female
                        </label>
              </Grid> 
              <Grid item xs={4}> 
              <label>Caste</label>        
                        <TextField
                          select
                          id="caste"
                          placeholder="enter Caste"
                          value={values.caste}
                          onChange={handleChange("caste")}
                          helperText={touched.caste ? errors.caste : ""}
                          error={touched.caste && Boolean(errors.caste)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {caste.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid> 
              <Grid item xs={4}> 
              <label>Category</label>        
                        <TextField
                          select
                          id="casteCategory"
                          placeholder="enter Category"
                          value={values.casteCategory}
                          onChange={handleChange("casteCategory")}
                          helperText={touched.casteCategory ? errors.casteCategory : ""}
                          error={touched.casteCategory && Boolean(errors.casteCategory)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {casteCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid> 
              <Grid item xs={4}> 
              <label>Religion</label>        
                        <TextField
                          select
                          id="religion"
                          placeholder="Religion"
                          value={values.religion}
                          onChange={handleChange("religion")}
                          helperText={touched.religion ? errors.religion : ""}
                          error={touched.religion && Boolean(errors.religion)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {religion.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid> 
              <Grid item xs={4}>  
                         <label>Unique Identification Number</label>     
                        <TextField 
                            id="uniqueIdNumber"
                            placeholder="Enter Unique Id Number"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.uniqueIdNumber}
                            fullWidth
                            helperText={touched.uniqueIdNumber && errors.uniqueIdNumber ? "has-error" : null}
                            error={touched.uniqueIdNumber && Boolean(errors.uniqueIdNumber)}
                          />
              </Grid> 
              <Grid item xs={4}> 
              <label>Nationality</label>        
                        <TextField
                          select
                          id="nationality"
                          placeholder="enter nationality"
                          value={values.nationality}
                          onChange={handleChange("nationality")}
                          helperText={touched.nationality ? errors.nationality : ""}
                          error={touched.nationality && Boolean(errors.nationality)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {nationality.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid>
              <Grid item xs={4}> 
              <label>Mother Tongue</label>        
                        <TextField
                          select
                          id="motherTongue"
                          placeholder="motherTongue"
                          value={values.motherTongue}
                          onChange={handleChange("motherTongue")}
                          helperText={touched.motherTongue ? errors.motherTongue : ""}
                          error={touched.motherTongue && Boolean(errors.motherTongue)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {motherTongue.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid>
              <Grid item xs={4}> 
              <label>Blood Group</label>        
                        <TextField
                          select
                          id="bloodGroup"
                          placeholder="enter blood group"
                          value={values.bloodGroup}
                          onChange={handleChange("bloodGroup")}
                          helperText={touched.bloodGroup ? errors.bloodGroup : ""}
                          error={touched.bloodGroup && Boolean(errors.bloodGroup)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {bloodGroup.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid>             
                       
            </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    firstName,
    midName,
    lastName,
    birthPlace,
    birthDate,
    uniqueIdNumber,
    nationality,
    motherTongue,
    caste,
    casteCategory,
    religion,
    gender,
    bloodGroup


  }) => {
    return {
      firstName: firstName || "",
      midName: midName || "",
      lastName: lastName || "",
      birthDate: birthDate || "",
      gender: gender || "",
      uniqueIdNumber: uniqueIdNumber || "",
      birthPlace: birthPlace || "",
      nationality: nationality || "",
      motherTongue: motherTongue || "",
      caste: caste || "",
      casteCategory: casteCategory || "",
      religion: religion || "",
      bloodGroup: bloodGroup || ""
    };
  },

  validationSchema: Yup.object().shape({
    firstName: Yup.string().required("first name is required"),
    lastName: Yup.string().required("last name is required"),
    student: Yup.string().required("Select student"),
    gender: Yup.string().required(" gender is required"),
    uniqueIdNumber: Yup.string().required("Must enter a id number")
}),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { withStyles, RadioGroup } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import MuiPhoneNumber from "material-ui-phone-number";
import Grid from '@material-ui/core/Grid';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
  } from "@material-ui/pickers";
  import DateFnsUtils from "@date-io/date-fns";
  import Radio from '@material-ui/core/Radio';
  
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/
const styles = () => ({
  card: {
    maxWidth: 900,
    marginTop: 20
  },
  container: {
    display: "Flex",
    justifyContent: "center"
  },
  actions: {
    float: "right"
  }
 
});

const relationCategory = [
  {
    value: "father",
    label: "Father"
  },
  {
    value: "mother",
    label: "Mother"
  },
  {
    value: "other",
    label: "Other"
  }
];
const courseCategory = [
    {
      value: "lkg",
      label: "LKG"
    },
    {
      value: "ukg",
      label: "UKG"
    },
    {
      value: "nursery",
      label: "Nursery"
    }
  ];

  const occupationCategory = [
    {
      value: "private",
      label: "Private"
    },
    {
      value: "government",
      label: "Government"
    },
    {
      value: "other",
      label: "Other"
    }
  ];


const form = props => {
    const [selectedValue, setSelectedValue] = React.useState('existingStudent');
    const handleOptionChange = (event) => {
        setSelectedValue(event.target.value);
      };
    const [selectedValue1, setSelectedValue1] = React.useState('male');
    const handleOptionChange1 = (event) => {
        setSelectedValue1(event.target.value);
      };
    const [selectedValue2, setSelectedValue2] = React.useState('existingParents');
    const handleOptionChange2 = (event) => {
        setSelectedValue2(event.target.value);
      };
    const minDate = new Date(Date.now());
   const trueBool = true;
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue
  } = props;

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <h1>Student </h1>
                  <label>
                    <Radio
                      checked={selectedValue === 'existingStudent'}
                      onChange={handleOptionChange}
                      value="existingStudent"
                      name="student"
                      inputProps={{ 'aria-label': 'Existing Student' }}
                    />
                      Existing Student
                  </label>
                  <label>
                    <Radio
                      checked={selectedValue === 'newStudent'}
                      onChange={handleOptionChange}
                      value="newStudent"
                      name="student"
                      inputProps={{ 'aria-label': 'New Student' }}
                    />
                      New Student
                  </label>
              </Grid>
              <Grid item xs={6}>
              <label>Date of Registration</label>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              margin="dense"
                              variant="outlined"
                              minDate={minDate}
                              id="registrationDate"
                              placeholder="Date of Registration"
                              format="MM/dd/yyyy"
                              onBlur={handleBlur}
                              fullWidth
                              value={values.registrationDate}
                              onChange={date => setFieldValue('registrationDate', date)}
                              helperText={touched.registrationDate ? errors.registrationDate : ""}
                              error={touched.registrationDate && Boolean(errors.registrationDate)}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}

                        />
                        </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}> 
              <label>Course</label>        
                        <TextField
                          select
                          id="course"
                          placeholder="Course"
                          value={values.course}
                          onChange={handleChange("course")}
                          helperText={touched.course ? errors.course : ""}
                          error={touched.course && Boolean(errors.course)}
                          margin="dense"
                          variant="outlined"
                        fullWidth
                          
                        >
                          {courseCategory.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>
              </Grid> 
              <Grid item xs={4}> 
              <label>First Name</label> 
                        <TextField
                          id="firstName"
                          placeholder="First Name"
                          value={values.firstName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.firstName ? errors.firstName : ""}
                          error={touched.firstName && Boolean(errors.firstName)}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                      
                        />
              </Grid> 
              <Grid item xs={4}>
              <label>Middle Name</label>
                        <TextField
                          id="midName"
                          placeholder="Middle Name"
                          value={values.midName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                        
                        />
              </Grid>
              <Grid item xs={4}>
                <label>Last Name</label>
                        <TextField
                          id="lastName"
                          placeholder="Last Name"
                          value={values.lastName}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          helperText={touched.lastName ? errors.lastName : ""}
                          error={touched.lastName && Boolean(errors.lastName)}
                          margin="dense"
                          variant="outlined"
                          fullWidth
                          
                          
                        />
              </Grid> 
              <Grid item xs={12}> 
                        <h3>Gender</h3>
                        <label>
                        <Radio
                            checked={selectedValue1 === 'male'}
                            onChange={handleOptionChange1}
                            value="male"
                            name="gender"
                            inputProps={{ 'aria-label': 'Male' }}
                        />
                            Male
                        </label>
                        <label>
                        <Radio
                            checked={selectedValue1 === 'female'}
                            onChange={handleOptionChange1}
                            value="female"
                            name="gender"
                            inputProps={{ 'aria-label': 'Female' }}
                        />
                          Female
                        </label>
              </Grid> 
              <Grid item xs={6}>
                <label>Date of Birth</label>
                          <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                              margin="dense"
                              variant="outlined"
                              fullWidth
                              id="birthDate"
                              placeholder="Date of Birth"
                              format="MM/dd/yyyy"
                              onBlur={handleBlur}
                              value={values.birthDate}
                              onChange={date => setFieldValue('birthDate', date)}
                              helperText={touched.birthDate ? errors.birthDate : ""}
                              error={touched.birthDate && Boolean(errors.birthDate)}
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}

                        />
                        </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}>  
                         <label>Contact Number</label>     
                        <MuiPhoneNumber 
                            id="studentContactNo"
                            placeholder="Phone Number"
                            defaultCountry={"us"}
                            variant="filled"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.studentContactNo}
                            fullWidth
                            helperText={touched.studentContactNo && errors.studentContactNo ? "has-error" : null}
                            error={touched.studentContactNo && Boolean(errors.studentContactNo)}
                          />
              </Grid> 
                        
                       
            </Grid>
          </CardContent>
          <CardActions className={classes.actions}>
            <Button type="submit" color="primary" disabled={isSubmitting}>
              SAVE
            </Button>
            <Button color="secondary" onClick={handleReset}>
              CLEAR
            </Button>
          </CardActions>
        </Card>
      </form>
    </div>
  );
};

const Form = withFormik({
  mapPropsToValues: ({
    firstName,
    midName,
    lastName,
    course,
    student,
    registrationDate,
    birthDate,
    studentContactNo,
    gender


  }) => {
    return {
      firstName: firstName || "",
      midName: midName || "",
      lastName: lastName || "",
      course: course || "",
      student: student || "",
      registrationDate: registrationDate || "",
      birthDate: birthDate || "",
      gender: gender || "",
     
    };
  },

  validationSchema: Yup.object().shape({
    firstName: Yup.string().required("first name is required"),
    lastName: Yup.string().required("last name is required"),
    course: Yup.string().required("Select your course "),
    student: Yup.string().required("Select student"),
    registrationDate: Yup.string().required(" registration date is required"),
    gender: Yup.string().required(" gender is required"),
    studentContactNo: Yup.string()
    .matches(phoneRegExp, "Phone number is not valid")
    .required("Must enter a phone number")
}),

  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      // submit to the server
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 1000);
  }
})(form);

export default withStyles(styles)(Form);
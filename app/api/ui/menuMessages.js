/*
 * Sidebar Component
 *
 * This contains all the text for the Sidebar Componen.
 */
import { defineMessages } from 'react-intl';

export const scope = 'edumations.menu';
export default defineMessages({
  // user details
  online: {
    id: `${scope}.status.online`,
    defaultMessage: 'Oniine',
  },
  idle: {
    id: `${scope}.status.idle`,
    defaultMessage: 'Idle',
  },
  bussy: {
    id: `${scope}.status.bussy`,
    defaultMessage: 'Bussy',
  },
  offline: {
    id: `${scope}.status.offline`,
    defaultMessage: 'Offline',
  },
  //home
  home: {
    id: `${scope}.home`,
    defaultMessage: 'Home',
  },
  applications: {
    id: `${scope}.home.applications`,
    defaultMessage: 'Applications',
  },
  todo: {
    id: `${scope}.home.applications.todo`,
    defaultMessage: 'To Do',
  },
  dashboard: {
    id: `${scope}.home.dashboard`,
    defaultMessage: 'Dashboard',
  },
  // timeline
  timeline: {
    id: `${scope}.home.timeline`,
    defaultMessage: 'Timeline',
  },


  // Academics
  academic: {
    id: `${scope}.academic`,
    defaultMessage: 'Academic',
  },
  academic_sessions: {
    id: `${scope}.academic.academic_sessions`,
    defaultMessage: 'Academic Sessions',
  },
  manage_academic_sessions: {
    id: `${scope}.academic.manage_academic_sessions`,
    defaultMessage: 'Manage Academic Sessions',
  },
  // Classes
  classes: {
    id: `${scope}.academic.classes`,
    defaultMessage: 'Classes',
  },
  all_classes: {
    id: `${scope}.academic.classes.all_classes`,
    defaultMessage: 'All Classes',
  },
  add_class: {
    id: `${scope}.academic.classes.add_class`,
    defaultMessage: 'Add Class',
  },
  // timetable
  timetable: {
    id: `${scope}.academic.timetable`,
    defaultMessage: 'Time Table',
  },
  manage_time_tables: {
    id: `${scope}.home.timetable.manage_time_tables`,
    defaultMessage: 'Manage Time Tables',
  },
// students
students: {
  id: `${scope}.students`,
  defaultMessage: 'Students',
},
// calender
calender: {
  id: `${scope}.calender`,
  defaultMessage: 'Calender',
},
//employees
employees: {
  id: `${scope}.employees`,
  defaultMessage: 'Employees',
},
//finance
finance: {
  id: `${scope}.finance`,
  defaultMessage: 'Finance',
},
//exams
exams: {
  id: `${scope}.exams`,
  defaultMessage: 'Exams',
},
//inventory
inventory: {
  id: `${scope}.inventory`,
  defaultMessage: 'Inventory',
},
//library
library: {
  id: `${scope}.library`,
  defaultMessage: 'Library',
},
//reception
reception: {
  id: `${scope}.reception`,
  defaultMessage: 'Reception',
},
//resources
resources: {
  id: `${scope}.resources`,
  defaultMessage: 'Resources',
},
//transport
transport: {
  id: `${scope}.transport`,
  defaultMessage: 'Transport',
},

});

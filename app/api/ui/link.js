module.exports = {
  dashboard: '/app',
  login: '/login',
  market: '#',
  task: '/app/pages/task',
  profile: '/app/pages/user-profile',
  calendar: '/app/pages/calendar',
  email: '/app/pages/email',
  twitter: '#',
  github: '#',
  pinterest: '#',
  linkedin: '#'
};

module.exports = [
  {
    key: 'home',
    name: 'Home',
    icon: 'home',
    child: [
        {
        key: 'landing_page',
        name: 'Landing Page',
        title: true,
      },
      {
        
        key: 'timeline',
        name: 'Timeline',
        icon: 'timeline',
        link: '/app/timeline'
      },
      {
        key: 'dashboard',
        name: 'Dashboard',
        icon: 'dashboard',
        link: '/app/dashboard'
      },
      {
        key: 'applications',
        name: 'Applications',
        title: true,
      },
      {
        key: 'todo',
        name: 'Todo',
        icon: 'check_circle',
        link: '/app/to-do'
      },
    ]
  },
  {
    key: 'reception',
    name: 'Reception',
     icon: 'personoutlinerounded',
     child: [
      {
        key: 'admission_enquiry',
        name: 'Admission Enquiry',
        title: true,
      },
      {
        key: 'admission_enquiry',
        name: 'Admission Enquiry',
        icon: 'school',
        link: '/app/reception/admission-enquiry'
      },
      {
        key: 'visitors',
        name: 'Visitors',
        title: true,
      },
      {
        key: 'visitor_log',
        name: 'Visitor Log',
        icon: 'school',
        link: '/app/reception/visitor-log'
      },
      {
        key: 'visitor_message',
        name: 'Visitor Message',
        icon: 'school',
        link: '/app/reception/visitor-message'
      },
      {
        key: 'call_log',
        name: 'Call Log',
        icon: 'phone square',
        link: '/app/reception/call-log'
      },
      {
        key: 'portal',
        name: 'Portal',
        title: true,
      },
      {
        key: 'portal_record',
        name: 'Portal Record',
        icon: 'school',
        link: '/app/reception/portal-record'
      },
      {
        key: 'complaint',
        name: 'Complaint',
        title: true,
      },
      {
        key: 'complaint',
        name: 'Complaint',
        icon: 'school',
        link: '/app/reception/complaint'
      },
      {
        key: 'pass',
        name: 'GatePass',
        title: true,
      },
      {
        key: 'gate_pass',
        name: 'Gate Pass',
        icon: 'school',
        link: '/app/reception/gate-pass'
      },
    
    ]
  },
  {
    key: 'academic',
    name: 'Academic',
    icon: 'school',
    child: [
      {
        key: 'academic_sessions',
        name: 'Academic Sessions',
        title: true,
      },
      {
        key: 'manage_academic_sessions',
        name: 'Manage Sessions',
        icon: 'school',
        link: '/app/academic/academic-sessions'
      },
      {
        key: 'classes',
        name: 'Classes',
        title: true,
      },
      {
        key: 'add_class',
        name: 'Add Class',
        icon: 'school',
        link: '/app/academic/add-class'
      },
      {
        key: 'all_classes',
        name: 'All Classes',
        icon: 'school',
        link: '/app/academic/all-classes'
      },
      {
        key: 'timetable',
        name: 'TimeTables',
        title: true,
      },
      {
        key: 'time-table',
        name: 'Manage Time Table',
        icon: 'school',
        link: '/app/academic/timetable/allocate'
      }
    ]
  },
  {
    key: 'students',
    name: 'Students',
     icon: 'school',
     child: [
       {
        key: 'file',
        name: 'File Upload',
        title: true,
       },
       {
        key: 'upload-file',
        name: 'Student File Upload ',
        icon: 'cloud_upload',
        link: '/app/student/upload-file'
      },
      {
        key: 'resistration',
        name: 'Registration',
        title: true,
      },
     
       {
        key: 'registration_list',
        name: 'Manage Registration ',
        icon: 'school',
        link: '/app/student/registration-list'
      },
       {
        key: 'student',
        name: 'Student ',
        title: true,
      },
      {
        key: 'student-list',
        name: 'Student Details',
        icon: 'school',
        link: '/app/student/student-list'
      },
       
      {
        key: 'id_card',
        name: 'ID Card',
        icon: 'id card ',
        link: '/app/students/id-card'
      },
      {
        key: 'img',
        name: 'Image Upload',
        icon: 'cloud_upload',
        link: '/app/student/upload-image'
      },
      {
        key: 'attendence',
        name: 'Attendence',
        title: true,
      },
      {
        key: 'attendence',
        name: 'Attendence',
        icon: 'school',
        link: '/app/students/attendence'
      },
      {
        key: 'promotion',
        name: 'Promotion',
        title: true,
      },
      {
        key: 'promotion',
        name: 'Promotion',
        icon: 'school',
        link: '/app/students/promotion'
      },

     ]
  },
  {
    key: 'employee',
    name: 'Employee',
    icon: 'personround',
    child: [
      {
        key: 'employee',
        name: 'Employee',
        title: true,
      },
      {
        key: 'emp_list',
        name: 'Employee List',
        icon: 'school',
        link: '/app/employee/employee-list'
      },
      {
        key: 'id_card',
        name: 'ID Card',
        icon: 'school',
        link: '/app/employee/id-card'
      },
      {
        key: 'leave',
        name: 'Leave',
        title: true,
      },
      {
        key: 'leave',
        name: 'Leave',
        icon: 'school',
        link: '/app/employee/leave'
      },
      {
        key: 'payroll',
        name: 'Payroll',
        title: true,
      },
      {
        key: 'payroll',
        name: 'Payroll',
        icon: 'school',
        link: '/app/employee/payroll'
      }
    ]
  },
  {
    key: 'exam',
    name: 'Exam',
    icon: 'subject',
    child: [
      {
        key: 'exams',
        name: 'Exams',
        title: true,
      },
      {
        key: 'exam_schedule',
        name: 'Exam Schedule',
        icon: 'school',
        link: '/app/exam/exam-schedule'
      },
      
      {
        key: 'online_exam',
        name: 'Online Exam',
        icon: 'school',
        link: '/app/exam/online-exam'
      },
      {
        key: 'records',
        name: 'Records',
        title: true,
      },
      {
        key: 'record_marks',
        name: 'Record Marks',
        icon: 'school',
        link: '/app/exam/record-marks'
      },
      {
        key: 'topper_report',
        name: 'Topper Report',
        icon: 'school',
        link: '/app/exam/topper-report'
      }
    ]
  },
  {
    key: 'transport',
    name: 'Transport',
    icon: 'local_shipping',
    child: [
      {
        key: 'transport',
        name: 'Transport',
        title: true,
      },
      {
        key: 'route',
        name: 'Route',
        icon: 'route',
        link: '/app/transport/route'
      },
      {
        key: 'vehicle',
        name: 'Vehicle',
        icon: 'bus',
        link: '/app/transport/vehicle'
      },
      {
        key: 'vehicle_incharge',
        name: 'Vehicle Incharge',
        icon: 'incharge',
        link: '/app/transport/vehicle-incharge'
      }
    ]
  },
  {
    key: 'finance',
    name: 'Finance',
    icon: 'account_balance',
    child: [
      {
        key: 'fee',
        name: 'Fees',
        title: true,
      },
      {
        key: 'fee_group',
        name: 'Fee Group',
        icon: 'group',
        link: '/app/finance/fee-group'
      },
      {
        key: 'fee_head',
        name: 'Fee Head',
        icon: 'add_a_photo',
        link: '/app/finance/fee-head'
      },
      {
        key: 'fee_concession',
        name: 'Fee Consession',
        icon: 'euro_symbol',
        link: '/app/finance/fee-concession'
      },
      {
        key: 'transport_fee',
        name: 'Transport Fee',
        icon: 'euro_symbol',
        link: '/app/finance/transport-fee'
      }
    ]
  },
  {
    key: 'calender',
    name: 'Academic Calender',
    icon: 'event',
    child: [
      {
        key: 'calender',
        name: 'Calender',
        title: true,
      },
      {
        key: 'holiday',
        name: 'Holiday',
        icon: 'favorite',
        link: '/app/calender/holiday'
      },
      {
        key: 'events',
        name: 'Events',
        icon: 'event',
        link: '/app/calender/events'
      }
    ]
  },
  {
    key: 'recources',
    name: 'Recources',
    icon: 'assignment',
    child: [
      {
        key: 'recources',
        name: 'Recources',
        title: true,
      },
      {
        key: 'assignment',
        name: 'Assignment',
        icon: 'assignment',
        link: '/app/recources/assignment'
      }
    ]
  },
  {
    key: 'library',
    name: 'Library',
    icon: 'library_books',
    child: [
      {
        key: 'library',
        name: 'Library',
        title: true,
      },
      {
        key: 'library',
        name: 'Library',
        icon: 'library_books',
        link: '/app/library/library'
      }
    ]
  }
];

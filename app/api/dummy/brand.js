module.exports = {
  name: 'Edumations',
  desc: 'Edumations - School Web App',
  prefix: 'edumations',
  footerText: 'Edumations All Rights Reserved 2020',
  logoText: 'Edumations',
};

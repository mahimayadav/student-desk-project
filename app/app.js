/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import '@babel/polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router/immutable';
import history from 'utils/history';
import 'sanitize.css/sanitize.css';
// Import root app
import App from 'containers/App';
import './styles/layout/base.scss';
// import Login from "enl-containers/pages/Authentication/Login"

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Load the favicon and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
/* eslint-enable import/no-unresolved, import/extensions */

import configureStore from './redux/configureStore';

// Import i18n messages
import { translationMessages } from './i18n';

// // Apollo Client
// import { resolvers, typeDefs } from './services/schema';
// import { ApolloProvider, useQuery} from '@apollo/react-hooks';
// import { ApolloClient } from 'apollo-client';
// import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
// import { HttpLink } from 'apollo-link-http';
// import gql from 'graphql-tag';


// const IS_LOGGED_IN = gql`
//   query IsUserLoggedIn {
//     isLoggedIn @client
//   }
// `;

// function IsLoggedIn() {
//   const { data } = useQuery(IS_LOGGED_IN);
//   return data.isLoggedIn ? <App /> : <Login />;
// }

// const cache = new InMemoryCache();
// const link = new HttpLink({
//   uri: 'http://localhost:4000/graphql/',
//    headers: {
//       authorization: localStorage.getItem('token'),
//     }, 
// });

// const client = new ApolloClient({
//   cache,
//   link,
//   typeDefs,
//   resolvers
// });

// // ........testing purpose
  



// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

const render = messages => {
  ReactDOM.render(
    
      // <ApolloProvider client={client}>
        <Provider store={store}>
          <LanguageProvider messages={messages}>
          <ConnectedRouter history={history}>
            {/* <IsLoggedIn /> */}
            <App />
          </ConnectedRouter>
        </LanguageProvider>
        </Provider>,
      // </ApolloProvider>,
  MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n', 'containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support

if (!window.Intl) {
  new Promise(resolve => {
    resolve(import('intl'));
  })
    .then(() => Promise.all([
      import('intl/locale-data/jsonp/en.js'),
      import('intl/locale-data/jsonp/de.js'),
    ])) // eslint-disable-line prettier/prettier
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}


// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}

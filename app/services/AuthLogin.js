import React from 'react';
import { useApolloClient, useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Login from "enl-containers/pages/Authentication/Login"


export const LOGIN_USER = gql`
  mutation login($email: String!) {
    login(email: $email)
  }
`;


export default function Login() {
  const client = useApolloClient();
  console.log(client)
  const [login, { loading, error }] = useMutation(
    LOGIN_USER,
    {
      onCompleted({ login }) {
        localStorage.setItem('token', login );
        client.writeData({ data: { isLoggedIn: true } });
      }
    }
  );

 if (loading) return <p>Loading,.... Please wait.</p>;
  if (error) return <p>An error occurred</p>;

  return <Login login={login} />;
}